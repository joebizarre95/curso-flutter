import 'package:flutter/material.dart';
import 'package:flutter_tutorial_project/views/LoginView.dart';
import 'package:flutter_tutorial_project/views/NewsView.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: 'login',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case 'login':
            return MaterialPageRoute(builder: (context) => new LoginView());
          case 'NewsList':
            return MaterialPageRoute(builder: (context) => new NewsView());
        }
      },
    );
  }
}