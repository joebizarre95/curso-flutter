

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';



class LoginView extends StatefulWidget {
    @override
    _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    var _Heigth = MediaQuery.of(context).size.height;
    var _Width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Container(
              width: _Width,
              height: _Heigth,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/imgs/backbit.png'),
                  colorFilter:
                    ColorFilter.mode(Colors.white, BlendMode.softLight),
                )
              ),
            ),
            Container(
              width:  _Width,
              height: _Heigth,
              color: Colors.white.withAlpha(1000)
            ),
            Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    top: 40,
                    left: 25,
                    right: 25
                  ),
                  height: _Heigth * 0.65,
                  width: _Width,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 15
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(130),
                      bottomRight: Radius.circular(130),
                    )
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome to",
                        style: GoogleFonts.signika(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        )
                      ),
                      Image.asset('assets/imgs/BitLogo.png',
                                  width: 80 ,
                                  height: 40,
                          fit:BoxFit.contain),
                      Text(
                        'Please login to continue',
                        style:GoogleFonts.signika(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: TextField(
                          cursorColor: Colors.pink,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.pinkAccent
                            ),
                            hintText: 'Username',
                            border: OutlineInputBorder(
                              borderRadius:  BorderRadius.circular(30)
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: _Heigth * 0.02,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: TextField(
                          obscureText: true,
                          cursorColor: Colors.pink,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.pinkAccent
                            ),
                            hintText: 'Password',
                            border: OutlineInputBorder(
                                borderRadius:  BorderRadius.circular(30)
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () {print('forgot passowrd');},
                          child: Text(
                            'Forgot password?',
                            style: TextStyle(
                              color: Colors.pink,
                            ),
                          )
                        )
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: ButtonTheme(
                          minWidth: _Width / 2,
                          child: RaisedButton(
                            onPressed: () { print('seleccionado');},
                            child: Text(
                              'LOGIN',
                              style: TextStyle(fontSize: 24),
                              textAlign: TextAlign.center,
                            ),
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(20,)
                                )
                            ),
                          )
                        ),
                      )
                    ],
                  ),
                ),
                Expanded( flex: 500, child: SizedBox()),
                Text('OR', style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.black54,
                )),
                Expanded(flex: 1000, child:SizedBox()),
                RaisedButton(onPressed: (){print('singup');},
                      child:Text('SINGUP'),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(
                              Radius.circular(20,)
                          ))),
                Expanded(flex: 1000, child:SizedBox()),
                Text('Elaborated by Santiago Sanchez :) @')
              ],
            ),
          ],
        )
      )
    );
  }
}